# Trabalho Final 

## POS Graduação - Testes de Software 

### Módulo Integração contínua

![alt text](docs/ci-cd.png)

Parabéns 

:trophy:  Patricia Gabriele lavandoski Good Colaco

Trabalho entregue !!!!

## Instruções

1 - Instalar o python na ultima versão;

2 - instalar as dependencias com o comando:

~~~
pip install -r requirements.txt
~~~

3 - Executar os testes com o comando:

~~~
pytest
~~~